import styled from 'styled-components';

export const DefaultList = styled.ul`
  list-style-type: none;
  margin: 0;
  padding: 0;
`;