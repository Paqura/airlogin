import React from 'react';
import * as Lang from './style';

export default () => {
  return (
    <div className="lang">
      <Lang.Toggler>
        England
        <Lang.TogglerIcon></Lang.TogglerIcon>
      </Lang.Toggler>
    </div>
  )
}
