import React from 'react';
import * as Logotype from './style';

export default () => {
  return (
    <Logotype.Root>
      <Logotype.Link href="/">Airlogica</Logotype.Link>
    </Logotype.Root>
  )
}
