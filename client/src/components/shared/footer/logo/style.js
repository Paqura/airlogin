import styled from 'styled-components';

export const Root = styled.div`
  flex-grow: 1;
`;

export const Link = styled.a`
  font-size: calc(var(--font-size)*1.125);
  font-weight: bold;
  letter-spacing: 2px;
  color: var(--primary-black);

  &:focus {
    outline: var(--focus-border);
  }

  &:hover,
  &:focus,
  &:active {
    text-decoration: none;
    color: var(--primary-black);
    transition: opacity 200ms ease-in-out;
    opacity: 0.8;
  }
`;