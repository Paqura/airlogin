import React, { Component } from 'react';
import {connect} from 'react-redux';
import * as FLoatFooter from './style';
import Logo from './logo';
import Lang from './lang';
import Currency from './currency';
import { FloatButton } from '../buttons';
import {toggleFooterState, moduleName} from '../../../ducks/footer';
class Footer extends Component {
  render() {
    const { isVisible } = this.props;
    return (
      <React.Fragment>
        <FloatButton isVisible={isVisible} action={this.props.toggleFooterState}/>
        <FLoatFooter.Root isVisible={isVisible}>
          <div className="container">
            <FLoatFooter.List>
              <Logo />
              <Lang />
              <Currency />
            </FLoatFooter.List>
          </div>
        </FLoatFooter.Root>        
      </React.Fragment>      
    )
  }
};

const mapStateToProps = state => ({
  isVisible: state[moduleName].isVisible
});

const mapDispatchToProps = ({
  toggleFooterState
});

export default connect(mapStateToProps, mapDispatchToProps)(Footer);
