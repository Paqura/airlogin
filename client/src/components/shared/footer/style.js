import styled from 'styled-components';

export const Root = styled.footer`   
  position: fixed;
  bottom: 0;
  left: 0;
  box-shadow: var(--shadow);
  width: 100%;
  background-color: var(--primary-white);  
  padding: calc(var(--spacer)*2);
  padding-bottom: calc(var(--spacer)*5);
  will-change: transform;
  transition: transform 320ms;
  transform: ${props => props.isVisible ? 'translateY(0%)' : 'translateY(100%)'};

  @media (max-width: 420px) {
    padding: calc(var(--spacer)*2) 0 calc(var(--spacer)*6) 0;    
  }
`;

export const List = styled.div`
  display: flex;
  align-items: center;

  @media (max-width: 420px) {
    flex-direction: column;
  }
`;