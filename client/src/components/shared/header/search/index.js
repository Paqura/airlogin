import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as HotelsDuck from '../../../../ducks/hotels';
import * as Search from './style';
import SearchButton from './SearchButton';
class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.innerRef = React.createRef();
  }

  handleClick = (evt) => {
    evt.preventDefault();
    const params = {
      name: this.innerRef.current.value
    }
    this.props.find(params, this.props.history);
  }

  render() {
    return (
      <Search.Root>   
        <Search.Subtitle>
          Traveling and enjoing with us
        </Search.Subtitle>  
        <Search.Field>
          <Search.Input 
            type="search" 
            id="search"
            name="search"
            placeholder="Enter some place"
            innerRef={this.innerRef}
          />
          <div className="ml-2">
            <SearchButton 
              text="Search"
              finding={this.props.finding}
              action={this.handleClick}
            />
          </div>          
        </Search.Field>            
      </Search.Root>
    )
  }
};

const mapStateToProps = state => ({
  finding: state[HotelsDuck.moduleName].finding
});

export default connect(mapStateToProps, null)(SearchBar);
