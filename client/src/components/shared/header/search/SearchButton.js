import React from 'react';
import * as Button from '../../buttons/style';

export default (props) => {
  return (
    <Button.SearchButton      
      type="button"
      onClick={props.action}
    >
      { props.finding ? 'Searching...' : props.text }
    </Button.SearchButton>
  )
}
