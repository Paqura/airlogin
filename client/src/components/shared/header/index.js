// @flow

import React from 'react';
import PropTypes from 'prop-types';
import * as Head from './styles';
import Logo from './logo';
import SearchBar from './search';
import Auth from './auth';

const Header = (props) => {
  const { isAuthenticated } = props;

  return (
    <Head.Root>      
      <div className="container h-100">
        <Head.Bagage>     
          <Head.BagageText>
            Why your bagage is empty still?
          </Head.BagageText>   
          <img 
            src="./img/bg.svg" 
            width="300" height="300" 
            alt="123"
          />
        </Head.Bagage>
        <Head.Nav>
          <Logo />   
          <Auth isAuthenticated={isAuthenticated}/>     
        </Head.Nav>
        <SearchBar 
          find={props.find} 
          {...props}
        />
      </div>     
    </Head.Root>   
  )
}

Header.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired
};

export default Header;
