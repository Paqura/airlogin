import styled from 'styled-components';

export const Root = styled.header`
  position: relative;
  background-color: var(--primary-color);
  height: 70vh; 
  margin-bottom: calc(var(--spacer)*4);
  @media (max-width: 420px) {
    min-height: 100%;
    height: 40vh;
  }
`; 

export const Nav = styled.nav`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 80px;
  width: 100%;

  @media (max-width: 420px) {
    flex-direction: column;
    height: calc(100% - var(--spacer)*4);
    justify-content: center;
    
  }
`; 

export const Bagage = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  @media (max-width: 420px) {
    display: none;
  }
`;

export const BagageText = styled.span`
  font-size: calc(var(--font-size) * 2.375);
  letter-spacing: 2px;
  display: block;
  margin-bottom: calc(var(--spacer)*2);
`;

