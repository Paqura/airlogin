import React from 'react';
import * as Auth from './style';
import { Link } from 'react-router-dom';

export default ({ isAuthenticated }) => {
  return (
    <Auth.Root>
      { !isAuthenticated 
        ? 
          <React.Fragment>
            <Auth.Item>
              <Link to="/login" style={Auth.LinkStyle}>Login</Link>
            </Auth.Item>
            <Auth.Item>
              <Link to="/register" style={Auth.LinkStyle}>Register</Link>
            </Auth.Item>
          </React.Fragment>
        :  
          <Auth.Item>
            <Link to="/cabinet" style={Auth.LinkStyle}>Cabinet</Link>
          </Auth.Item>
      }
    </Auth.Root>
  )
};
