import styled from 'styled-components';

export const Shadower = styled.div`
  box-shadow: var(--card-shadow);
  &:hover {
    box-shadow: var(--card-hover-shadow);
  }
`;