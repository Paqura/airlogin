// @flow

import React from 'react';
import * as Toast from './style';

type Props = {
  message: string,
  action: () => void
}

const handleAction = action => {
  setTimeout(action, 2000);
}

export default (props: Props) => {
  const { message, action } = props;
  return (
    <Toast.Root>
      { message }
      { handleAction(action) }
    </Toast.Root>
  )
}
