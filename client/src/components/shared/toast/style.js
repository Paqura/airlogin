import styled from 'styled-components';

export const Root = styled.div`
    box-shadow: var(--auth-shadow);
    position: absolute;
    border-radius: 2px;
    top: 35px;
    width: auto;
    margin-top: 10px;
    max-width: 100%;
    height: auto;
    min-height: 48px;
    line-height: 1.5em;
    background-color: #323232;
    padding: 10px 25px;
    font-size: 1.1rem;
    font-weight: 300;
    color: #fff;
    display: flex;
    align-items: center;
    justify-content: space-between;
`;