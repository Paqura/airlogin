import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Loader from './index';
Enzyme.configure({ adapter: new Adapter() });

describe('Loader Test Group', () => {
  it('should be render without errors', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Loader />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('shallow test', () => {
    const element = shallow(
      <Loader />
    );
    expect(element.find('img')).toHaveLength(1);
  });

  test('should be return right tree', () => {
    const component = renderer.create(
      <Loader />
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
