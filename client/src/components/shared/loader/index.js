import React from 'react';
import styled, { keyframes } from 'styled-components';

const loadingAnimate = keyframes`
  from {
    transform: translateY(-6px);
  }
  to {
    transform: translateY(6px);
  }
`;

export const LoaderWrapper = styled.div`  
  animation: ${loadingAnimate} 4s ease infinite;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 120px;
  margin: 0 auto;
  will-change: transform;
  margin: 16px 0;
`;

export default () => {
  return (
    <LoaderWrapper>
      <img 
        src="./img/cloud.svg"
        width="60" 
        height="60" 
        alt="Loading..."
      />
    </LoaderWrapper>
  )
}
