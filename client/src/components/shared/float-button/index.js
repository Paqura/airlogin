// @flow

import React from 'react';
import * as Float from './style';
import PropTypes from 'prop-types';

type Props = {
  action: () => void,
  isVisible: boolean
}

const FloatButton = (props: Props) => {
  return (
    <Float.Root>
      <Float.Button 
        onClick={ props.action } 
        isVisible={props.isVisible}
      >
        { !props.isVisible ? 
          'Currency, terms and another' : 
          'Close' 
        }
      </Float.Button>
    </Float.Root>
  )
}

FloatButton.propTypes = {
  onClick: PropTypes.func,
  isVisible: PropTypes.bool.isRequired
}

export default FloatButton;