import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import FloatButton from './index';

describe('Float Button Render Test', () => {
  it('should be render without errors', () => {
    const div = document.createElement('div');
    ReactDOM.render(<FloatButton />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  test('есть корректный снимок', () => {
    const action = () => console.log('123');
    const component = renderer.create(
      <FloatButton action={action}/>
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});