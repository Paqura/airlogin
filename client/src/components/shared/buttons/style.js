import styled from 'styled-components';

export const Default = styled.button`
  cursor: pointer;
  background-color: transparent;
  border-radius: 2px;
  font-size: var(--font-size);
  border: none;
  &:focus {
    outline: none;
  }  
`; 

export const SearchButton = styled(Default)`
  border: 1px solid var(--primary-black);
  height: 100%;
  padding: 0 var(--spacer);  
  letter-spacing: 1px;
  &:hover {
    background-color: rgba(0, 0, 0, 0.1);
  }
  &:active, 
  &:focus {
    outline: var(--focus-border);
  }
`;  

export const OrderButton = styled(Default)`
  padding: calc(var(--spacer)/2) var(--spacer);
  max-width: 100%;
  background-color: var(--primary-color);
  color: var(--primary-white);
  font-size: calc(var(--font-size)*1.125);
  font-weight: 500;
  letter-spacing: 1px;
  border: 2px solid var(--primary-color);

  &:hover {
    background-color: var(--primary-white);
    color: var(--primary-color);
  }

  @media (max-width: 420px) {
    width: 100%;
  }
`;

export const FormButton = styled(Default)`
  padding: calc(var(--spacer)/2) var(--spacer);
  background-color: var(--primary-blue);
  width: 100%;
  color: var(--primary-white);
  letter-spacing: 1px;
  font-size: 20px;

  &:disabled {
    opacity: 0.4;
    cursor: default;
  }
`;