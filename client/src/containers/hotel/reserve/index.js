import React from 'react';
import * as Reserve from './style';

export default ({ price }) => {
  return (
    <Reserve.Root>
      <Reserve.ReservePrice>
        {price}₽
        <Reserve.PostPrice>
          in a day
        </Reserve.PostPrice>
      </Reserve.ReservePrice>
      <Reserve.ReserveButton>
        Reserve this place
      </Reserve.ReserveButton>
    </Reserve.Root>
  )
}
