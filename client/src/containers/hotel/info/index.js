import React from 'react';
import FacilitiesList from './facilities';
import * as Facilities from './style';

export default (props) => {
  const facilities = props.info.facilities[0];
  return (
    <Facilities.Root>
      <Facilities.Head>
        <h4>Facilities</h4>
      </Facilities.Head>
      <FacilitiesList 
        facilities={facilities}
      />
    </Facilities.Root>
  )
}
