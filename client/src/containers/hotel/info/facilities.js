import React from 'react';
import * as Facilities from './style';

const views = [
  { 
    name: 'beds',
    counter: true,
    props: 'bedsCount'
  },
  {
    name: 'wifi',
    counter: false
  },
  {
    name: 'parking',
    counter: false
  },
  {
    name: 'tv',
    counter: false
  },
  {
    name: 'kitchen',
    counter: false
  }
];

const renderFacilities = (view, facilities) => {
  const url = `./img/${view.name}.svg`;
  return(
    <Facilities.Item key={view.name}>
      <div>
        <Facilities.Icon 
          src={url}
          alt={view.name}
        />
      </div>
      <Facilities.IconDescription>
        { view.name }:  
        { view.counter ? 
          ` ${facilities[view.props]}` : 
            facilities[view.name] ? 
              ' yes' : 
              ' no'
        }
      </Facilities.IconDescription>  
    </Facilities.Item>  
  )
}

export default ({ facilities }) => {
  return (
    <Facilities.List>
      { views.map(view => renderFacilities(view, facilities)) }        
    </Facilities.List>
  )
}
