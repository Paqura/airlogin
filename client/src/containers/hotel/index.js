import React, { Component } from 'react';
import Loadable from 'react-loadable';
import Header from './header';
import Album from './album';
import Info from './info';
import HotelPlacemark from './map';
import HotelReserve from './reserve';
import HotelComment from './comments';
import CircleLoader from '../../components/shared/circle-loader';


const LoadableSlider = Loadable({
  loader: () => import('./slider'),
  loading: CircleLoader,
});
class Hotel extends Component {
  state = {
    sliderVisible: false
  }
  
  openSlider = (evt) => {
    this.setState({
      sliderVisible: true
    })
  }

  closeSlider = (evt) => {
    this.setState({
      sliderVisible: false
    })
  }

  render() {
    return (
      <div>
        { this.state.sliderVisible && 
          <LoadableSlider 
              closeSlider={this.closeSlider} 
          /> 
        }        
        <Header isAuthenticated={this.props.isAuthenticated}/>
        <div className="container">          
          <Album 
            picture={this.props.hotel.picture} 
            title={this.props.hotel.title}
            sliderVisible={this.state.sliderVisible}
            openSlider={this.openSlider}
          />
          <div className="row m-32">
            <div className="col-12 col-md-6">
              <Info 
                info={this.props.hotel}
              />
              <HotelComment                
              />
            </div>
            <div className="col-12 col-md-6 sticky-container">
              <HotelPlacemark 
                hotel={this.props.hotel}
              />
              <HotelReserve 
                price={this.props.hotel.price}
              />
            </div>
          </div>         
        </div>       
      </div>
    )
  }
}

export default Hotel;

export {
  Header,
  Album
}
