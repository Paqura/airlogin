import styled from 'styled-components';
import { Default } from "../../../components/shared/buttons/style";

export const SliderWrapper = styled.section`
  position: fixed;
  top: 0;
  left: 0;
  background-color: rgba(0, 0,0, 0.3);
  height: 100%;
  width: 100%;
  z-index: 101;
`;

export const SliderInner = styled.div`
  max-width: 800px;
  margin: 200px auto;
`;

export const CloseButton = styled(Default)`
  position: absolute;
  right: var(--spacer);
  top: var(--spacer);
  transition: transform 200ms;

  &:hover {
    transform: rotate(0.5turn);
  }

  & img {
    width: 64px;
    height: 64px;
  }
`;