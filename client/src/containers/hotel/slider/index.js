import React, { Component } from "react";
import Slider from "react-slick";
import * as CustomSlider from './style';

class SimpleSlider extends Component {
  componentDidMount() {
    document.body.classList.add('is-fix');
  }

  componentWillUnmount() {
    document.body.classList.remove('is-fix');
  }

  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };

    const slides = [
      'https://a0.muscache.com/im/pictures/1faf9a4c-f839-44da-bd37-65ddc928379e.jpg?aki_policy=xx_large',
      'https://a0.muscache.com/im/pictures/1faf9a4c-f839-44da-bd37-65ddc928379e.jpg?aki_policy=xx_large',
      'https://a0.muscache.com/im/pictures/1faf9a4c-f839-44da-bd37-65ddc928379e.jpg?aki_policy=xx_large',
    ];

    return (
      <CustomSlider.SliderWrapper>
        <CustomSlider.SliderInner>
          <CustomSlider.CloseButton onClick={this.props.closeSlider}>
            <img src="./img/close.svg" alt="Закрыть окно"/>
          </CustomSlider.CloseButton>
          <Slider {...settings}>        
            {slides.map(it => <div key={it}><img src={it} alt="Фото"/></div>  )}           
          </Slider>
        </CustomSlider.SliderInner>       
      </CustomSlider.SliderWrapper>      
    );
  }
}

export default SimpleSlider;