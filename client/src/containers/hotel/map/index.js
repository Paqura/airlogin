import React from 'react';
import { YMaps, Map, Placemark } from 'react-yandex-maps';
import styled from 'styled-components';

export const Root = styled.div`
  padding: var(--spacer);
  box-shadow: var(--card-shadow);
`;

const mapState = {center: [55.751574, 37.573856], zoom: 12 };

const HotelPlacemark = ({ hotel }) => (
  <Root>
    <YMaps>
      <Map state={mapState} width="100%"> 
        <Placemark
          geometry={{
            coordinates: [55.751574, 37.573856]
          }}
          properties={{
            balloonContent: `${hotel.title}`
          }}
          options={{
            iconLayout: 'default#image',
            iconImageSize: [30, 42],
            iconImageOffset: [-3, -42]
          }}
        /> 
      </Map>
    </YMaps>
  </Root>
);

export default HotelPlacemark;
