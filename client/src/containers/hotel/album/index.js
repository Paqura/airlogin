import React from 'react';
import * as Album from './style';
import * as Button from '../../../components/shared/buttons/style';

export default (props: Props) => {
  return (
    <Album.Root>
      <Album.InfoWrapper>
        <Album.InfoTitle>
          {props.title}
        </Album.InfoTitle>
        <Album.Description>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero, facilis necessitatibus rem sapiente dolor in excepturi nisi amet dolores. Rerum!
        </Album.Description>
        <Album.Description>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero, facilis necessitatibus rem sapiente dolor in excepturi nisi amet dolores. Rerum!
        </Album.Description>
        <p className="mt-4">
          <Button.OrderButton
            onClick={props.openSlider}
          >
            Check more photos
          </Button.OrderButton>
        </p>
      </Album.InfoWrapper>
      <Album.ImageWrapper>
        <Album.FullScreenPicture        
          src={props.picture}
          alt="Фотография отеля"
        />
      </Album.ImageWrapper>
    </Album.Root>
  )
}
