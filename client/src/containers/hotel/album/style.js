import styled from 'styled-components';

export const Root = styled.div`
  display: flex;
  justify-content: space-between;
  padding: calc(var(--spacer)*2);
  box-shadow: var(--card-shadow);

  @media (max-width: 420px) {
    display: flex;
    flex-direction: column;
    padding: var(--spacer) 0;
    padding-top: 0;
  }
`;

export const InfoWrapper = styled.div`  
  padding-right: var(--spacer);

  @media (max-width: 420px) {
    padding-right: 0;
    padding: var(--spacer);
  }  
`;

export const InfoTitle = styled.h1`
  font-size: calc(var(--font-size)*1.75);
  margin-bottom: calc(var(--spacer)*2);
  color: var(--primary-black);

  @media (max-width: 420px) {
    font-size: calc(var(--font-size)*1.375);
    margin-bottom: var(--spacer);
  } 
`;

export const Description = styled.p`
  font-size: calc(var(--font-size) * 1.125);
  color: var(--primary-black);
  margin-top: var(--spacer);

  @media (max-width: 420px) {
    font-size: var(--font-size);
  }
`;


export const ImageWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  @media (max-width: 420px) {
    order: -1;
    margin-bottom: var(--spacer);
  }

`;

export const FullScreenPicture = styled.img`
  display: block;
  max-width: 100%;
  height: auto;
  object-fit: cover;
  box-shadow: var(--shadow);
  
  @media (min-width: 420px) {
    min-width: 500px;
  }

  @media (max-width: 420px) {
    box-shadow: none;
  }
`;