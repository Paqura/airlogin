import styled from 'styled-components';
import * as Header from '../../../components/shared/header/styles';

export const Root = styled(Header.Root)`
  height: auto;
  margin-bottom: calc(var(--spacer)*2);
  background-color: transparent;
  box-shadow: var(--card-shadow);

  @media (max-width: 420px) {
    padding: var(--spacer) 0;
    margin-bottom: var(--spacer);
  }
`;

export const Nav = styled(Header.Nav)`
  & a {
    color: var(--primary-black) !important;
  }
`;
