// @flow

import React from 'react';
import * as HotelHeader from './style';
import Logo from '../../../components/shared/header/logo';
import Auth from '../../../components/shared/header/auth';

export default(props: Props) => {
  const {isAuthenticated} = props;

  return (
    <HotelHeader.Root>      
      <div className="container h-100">        
        <HotelHeader.Nav>
          <Logo />   
          <Auth 
            isAuthenticated={isAuthenticated}
          />     
        </HotelHeader.Nav>
      </div>     
    </HotelHeader.Root>   
  )
}
