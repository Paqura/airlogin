import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import Enzyme, { shallow } from 'enzyme';
import { HashRouter as Router } from 'react-router-dom';
import Adapter from 'enzyme-adapter-react-16';
import { Header, Album } from './index';
Enzyme.configure({ adapter: new Adapter() });

describe('Hotel tests', () => {
  it('should be render the Header without errors', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Router><Header /></Router>, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  test('should be return right snapshot', () => {
    const component = renderer.create(
      <Router>
        <Header />
      </Router>
      
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should be render the Albut without errors', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Router><Album /></Router>, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  test('should be return right snapshot', () => {
    const component = renderer.create(
      <Router>
        <Album />
      </Router>
      
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});