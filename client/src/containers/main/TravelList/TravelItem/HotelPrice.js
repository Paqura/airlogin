// @flow

import React from 'react';
import * as Hotel from './style';

type Props = {
  price: number,
};

export default (props: Props) => {
  const { price } = props;
  return (
    <div>
      <Hotel.Price>
        <b>{ price } ₽ </b> за сутки
      </Hotel.Price>
    </div>
  )
}
