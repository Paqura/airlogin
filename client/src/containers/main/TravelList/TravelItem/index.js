// @flow

import * as React from 'react';
import { Link } from 'react-router-dom';
import HotelPicture from './HotelPicture';
import HotelTitle from './HotelTitle';
import HotelPrice from './HotelPrice';
import * as Travel from './style';

import { type ITravel } from '../../../../types';

type Props = {
  travel: ITravel,
};

export default (props: Props) => {  
  const {travel} = props; 
  const url = `/hotel/${travel._id}`;
  return (
    <Travel.Root>
      <Link to={url}>
        <HotelPicture 
          pictureSrc={travel.picture}
        />
        <HotelTitle 
          title={travel.title}
          id={travel._id}
        />
        <HotelPrice 
          price={travel.price}
        />
      </Link>      
    </Travel.Root>
  )
}
