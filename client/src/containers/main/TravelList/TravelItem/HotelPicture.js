// @flow

import React from 'react';
import { Background, BackgroundWrapper } from './style';
import LazyLoad from 'react-lazyload';

type Props = {
  pictureSrc: string
};

export default (props: Props) => {
  return (
    <BackgroundWrapper>
      <LazyLoad height={200} >
        <Background 
          src={props.pictureSrc} 
          alt="Hotel"
        />
      </LazyLoad>
    </BackgroundWrapper>
  )
}
