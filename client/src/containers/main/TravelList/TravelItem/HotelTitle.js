// @flow

import React from 'react';
import * as Hotel from './style';

type Props = {
  title: string,
  id: string
}

export default (props: Props) => {
  const { title } = props;  
  return <Hotel.Title>{ title }</Hotel.Title>
}
