import styled from 'styled-components';
import { DefaultList } from '../../../components/shared/list/style';

export const Root = styled.section`
  padding: calc(var(--spacer)*4) 0;
  @media (max-width: 420px) {
    padding: var(--spacer) 0;
  }
`;

export const List = styled(DefaultList)`
  display: flex;
  flex-wrap: wrap;
  margin-left: -16px;
  margin-right: -16px;
`;