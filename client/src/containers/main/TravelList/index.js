import React, { Component } from 'react';
import { connect } from 'react-redux';
import TravelItem from './TravelItem';
import Loader from '../../../components/shared/loader';
import * as HotelDuck from '../../../ducks/hotels';

import * as Travel from './style';

class TravelList extends Component { 
  render() {
    const { travels, isLoading, lazyLoading } = this.props;
    return (   
      <div className="container">  
        <Travel.Root>
          { isLoading ? 
            <Loader /> : 
            <Travel.List>
              { travels && 
                travels.map(travel => 
                  <TravelItem 
                    key={travel._id} 
                    travel={travel} 
                  /> )}              
              { lazyLoading && 
                <div className="w-100 d-flex justify-content-center">
                  <Loader />
                </div> }
            </Travel.List>            
          }
        </Travel.Root> 
      </div>
    )
  }  
}

const mapStateToProps = state => ({
  isLoading: state[HotelDuck.moduleName].isLoading  
});

export default connect(mapStateToProps, null)(TravelList);
