import React, { Component } from 'react';
import * as Search from '../../components/shared/header/search/style';
import * as SearchPage from './style';
import SearchButton from '../../components/shared/header/search/SearchButton';


class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.innerRef = React.createRef();
  }

  handleClick = (evt) => {
    evt.preventDefault();
    const params = {
      name: this.innerRef.current.value
    }
    this.props.find(params, this.props.history);
  }

  render() {
    return (
      <SearchPage.Root>   
        <Search.Subtitle>
          Traveling and enjoing with us
        </Search.Subtitle>  
        <Search.Field>
          <Search.Input 
            type="search" 
            id="search"
            name="search"
            placeholder="Enter some place"
            innerRef={this.innerRef}
          />
          <div className="ml-2">
            <SearchButton 
              text="Search"
              action={this.handleClick}
            />
          </div>          
        </Search.Field>            
      </SearchPage.Root>
    )
  }
}

export default SearchBar;
