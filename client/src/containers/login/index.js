import React from 'react'
import { Field, reduxForm } from 'redux-form';
import validate from '../../utils/validate';
import CustomField from '../../components/shared/text-field';
import * as AuthForm from '../../components/shared/form/style';
import { FormButton } from '../../components/shared/buttons/style';

const LoginForm = props => {
  const { handleSubmit, pristine, submitting, invalid } = props;
  return (
    <AuthForm.Background>
      <AuthForm.FormRoot onSubmit={handleSubmit}>    
        <header>
          <AuthForm.Title>Login</AuthForm.Title>  
        </header>             
        <Field
          name="email"
          component={CustomField}
          type="email"            
          id="register-email"
          label="email"     
        />        
        <Field
          name="password"
          component={CustomField}
          type="password"
          id="register-password"
          label="password"     
        />
        <div>
          <FormButton 
            type="submit" 
            disabled={pristine || submitting || invalid}
          >
            { props.submitState ? 'Submiting' : 'Submit' }
          </FormButton>       
        </div>
      </AuthForm.FormRoot>
    </AuthForm.Background>
  )
}

export default reduxForm({
  form: 'login',
  validate
})(LoginForm)