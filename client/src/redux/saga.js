import { all } from 'redux-saga/effects';

import { saga as authSaga } from '../ducks/auth';
import { saga as hotelsSaga } from '../ducks/hotels';
import { saga as hotelSaga } from '../ducks/hotel';
import { saga as footerSaga } from '../ducks/footer';

export const saga = function * () {
  yield all([
    hotelsSaga(),
    hotelSaga(),
    footerSaga(),
    authSaga()
  ])
};
