import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';

import * as AuthDuck from '../ducks/auth';
import * as HotelsDuck from '../ducks/hotels';
import * as HotelDuck from '../ducks/hotel';
import * as FooterDuck from '../ducks/footer';

export default combineReducers({
  router,
  [AuthDuck.moduleName]: AuthDuck.reducer,
  [HotelsDuck.moduleName]: HotelsDuck.reducer,
  [HotelDuck.moduleName]: HotelDuck.reducer,
  [FooterDuck.moduleName]: FooterDuck.reducer,
  form: formReducer
})