// @flow

export type ITravel = {
  pic: string,
  title: string,
  price: number,
};

export type IField = {
  type: string,
  placeholder?: string,
  value?: string | number,
  name: string
};