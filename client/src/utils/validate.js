import validator from 'validator';

export default (values) => {
  const errors = {};
  
  if(!values.name) {
    errors.name = 'Field is required';
  }
  
  if(!values.email) {
    errors.email = 'You are remembere to enter email';
  }
  if(values.email && !validator.isEmail(values.email)) {
    errors.email = 'Email not valid. See to example: hello@gmail.com';
  }

  if(!values.password) {
    errors.password = 'Password is required';
  }

  return errors;
}