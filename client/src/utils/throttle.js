import throttle from 'lodash.throttle';

export const throttleFn = (fn, args) => {
  return throttle(fn.bind(this, args), 60);
};