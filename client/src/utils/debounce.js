import debounce from 'lodash.debounce';

export const debounceFn = (fn, args) => {
  return debounce(fn.bind(this, args), 200);
};