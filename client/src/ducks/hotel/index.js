import axios from 'axios';
import { Record } from 'immutable';
import { put, call, all, takeEvery } from 'redux-saga/effects';

export const moduleName = 'hotel';

// SELECTORS

// TYPES

export const FETCH_ONE_HOTEL_REQUEST = 'FETCH_ONE_HOTEL_REQUEST';
export const FETCH_ONE_HOTEL_SUCCESS = 'FETCH_ONE_HOTEL_SUCCESS';
export const FETCH_ONE_HOTEL_FAILURE = 'FETCH_ONE_HOTEL_FAILURE';

export const CLEAR_HOTEL_STATE = 'CLEAR_HOTEL_STATE';

// ACTIONS CREATURES

export const fetchOneHotel = (id) => {
  return {
    type: FETCH_ONE_HOTEL_REQUEST,
    payload: {
      id
    }
  }
};

export const clearHotelState = () => {
  return {
    type: CLEAR_HOTEL_STATE
  }
}

// SAGA

export const fetchOneHotelSaga = function* (action) {  
  const hotelID = action.payload.id;  
  
  const response = yield call(axios.get, `/api/hotel/${hotelID}`);

  try {
    const { data } = response;

    yield put({
      type: FETCH_ONE_HOTEL_SUCCESS,
      payload: data
    })

  } catch (error) {
    yield put({
      type: FETCH_ONE_HOTEL_FAILURE,
      payload: error.response.data.message
    })
  }
};

export const saga = function* () {
  yield all([
    takeEvery(FETCH_ONE_HOTEL_REQUEST, fetchOneHotelSaga)
  ])
};

// REDUCER

const HotelSchema = Record({
  isLoading: false,
  info: null
});

export const reducer = (state = new HotelSchema(), action) => {
  const { type, payload } = action;

  switch(type) {
    case FETCH_ONE_HOTEL_REQUEST:
      return state
        .set('isLoading', true);
    case FETCH_ONE_HOTEL_SUCCESS:
      return state
        .set('info', payload)
        .set('isLoading', false);

    case CLEAR_HOTEL_STATE:
      return state
        .set('info', null)

    default: 
      return state;
  }
};
