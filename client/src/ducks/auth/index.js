import { put, all, call, takeEvery } from 'redux-saga/effects';
import { Record } from 'immutable';
import axios from 'axios';
import jwt_decode from 'jwt-decode';

import setAuthToken from '../../utils/setAuthToken';
import isEmpty from '../../utils/isEmpty';

export const moduleName = 'auth';

// TYPES

export const LOGIN_FORM_REQUEST = 'LOGIN_FORM_REQUEST';
export const LOGIN_FORM_SUCCESS = 'LOGIN_FORM_SUCCESS';
export const LOGIN_FORM_FAILURE = 'LOGIN_FORM_FAILURE';

export const REGISTER_FORM_REQUEST = 'REGISTER_FORM_REQUEST';
export const REGISTER_FORM_SUCCESS = 'REGISTER_FORM_SUCCESS';
export const REGISTER_FORM_FAILURE = 'REGISTER_FORM_FAILURE';

export const RESET_STATE_OF_ERROR = 'RESET_STATE_OF_ERROR';

// ACTION CREATERS

export const loginFormRequest = (values, history) => {
  return {
    type: LOGIN_FORM_REQUEST,
    payload: {
      ...values,
      history
    }
  }
};

export const registerFormRequest = (values, history) => {
  return {
    type: REGISTER_FORM_REQUEST,
    payload: {
      ...values,
      history
    }
  }
};

export const setCurrentUser = decoded => {
  return {
    type: LOGIN_FORM_SUCCESS,
    payload: decoded
  };
};

export const resetStateOfError = () => {
  return {
    type: RESET_STATE_OF_ERROR
  }
}

export const logoutUser = async function()  {
  localStorage.removeItem('jwtToken');
  setAuthToken(false);
  await setCurrentUser({});
};

// SAGAS

export const loginFormRequestSaga = function* ({payload}) { 
  const history = payload.history;
  const params = {
    email: payload.email,
    password: payload.password
  };
  
  try {
    const response = yield call(axios.post, '/api/auth/login', params);
    const { token } = response.data;
    const decoded = jwt_decode(token); 
    
    localStorage.setItem("jwtToken", token);
    setAuthToken(token);

    yield put({
      type: LOGIN_FORM_SUCCESS,
      payload: decoded
    });   

    history.push('/');

  } catch (error) {
    const errorMessage = error.response.data.message;

    yield put({
      type: LOGIN_FORM_FAILURE,
      payload: errorMessage
    })   
  }  
};

export const registerFormRequestSaga = function* ({payload}) { 
  const history = payload.history;
  const params = {
    name: payload.name,
    email: payload.email,
    password: payload.password
  };  
  
  try {    
    const response = yield call(axios.post, '/api/auth/register', params);  
    
    yield put({
      type: REGISTER_FORM_SUCCESS,
      payload: {
        name: response.data.name,
        message: 'Регистрация прошла успешно'
      }
    })

    history.push('/login');

  } catch (error) {    
    yield put({
      type: REGISTER_FORM_FAILURE,
      payload: error.response.data.message || error
    })
  }
};

export const saga = function* () {
  yield all([
    takeEvery(LOGIN_FORM_REQUEST, loginFormRequestSaga),
    takeEvery(REGISTER_FORM_REQUEST, registerFormRequestSaga),
  ])
};

// REDUCER

const AuthSchema = Record({
  isAuthenticated: false,
  user: {},
  error: null,
  submit: false
});

export const reducer = (state = new AuthSchema(), action) => {
  const { type, payload } = action;
  switch(type) {
    case LOGIN_FORM_REQUEST:
      return state
        .set('submit', true);

    case LOGIN_FORM_SUCCESS: 
      return state
        .set('isAuthenticated', !isEmpty(payload))
        .set('error', null)
        .set('user', payload)
        .set('submit', true);

    case LOGIN_FORM_FAILURE:
      return state
        .set('error', payload)
        .set('submit', false);

    case REGISTER_FORM_REQUEST:
      return state
        .set('submit', true);
    case REGISTER_FORM_SUCCESS:
      return state
        .set('submit', true);
    case REGISTER_FORM_FAILURE:
      return state
        .set('submit', false);

    case RESET_STATE_OF_ERROR:
      return state
        .set('error', null);    

    default:
      return state;  
  }
}