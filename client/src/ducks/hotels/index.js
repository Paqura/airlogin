import { put, all, call, takeEvery, select } from 'redux-saga/effects';
import { Record} from 'immutable';
import axios from 'axios';

export const moduleName = 'hotels';

// SELECTORS

export const getHotelList = state => state.get('hotels');

export const getPayload = (list, payload) => {
  const id2 = payload.map(it => it._id);  
  return list.filter(element => id2.includes(element._id));
}

export const getFilteredList = (list, payload) => {
  const id2 = payload.map(it => it._id);
  console.log(list);
  return list.filter(element => id2.includes(element._id));
};

// TYPES

export const FETCH_HOTELS_REQUEST = 'FETCH_HOTELS_REQUEST';
export const FETCH_HOTELS_SUCCESS = 'FETCH_HOTELS_SUCCESS';
export const FETCH_HOTELS_FAILURE = 'FETCH_HOTELS_FAILURE';

export const FETCH_LAZY_HOTELS_REQUEST = 'FETCH_LAZY_HOTELS_REQUEST';
export const FETCH_LAZY_HOTELS_SUCCESS = 'FETCH_LAZY_HOTELS_SUCCESS';
export const FETCH_LAZY_HOTELS_FAILURE = 'FETCH_LAZY_HOTELS_FAILURE';

export const FIND_HOTELS_REQUEST = 'FIND_HOTELS_REQUEST';
export const FIND_HOTELS_SUCCESS = 'FIND_HOTELS_SUCCESS';
export const FIND_HOTELS_FAILURE = 'FIND_HOTELS_FAILURE';

// ACTION CREATERS

export const fetchHotelsRequest = () => {
  return {
    type: FETCH_HOTELS_REQUEST
  }
};

export const fetchLazyHotelsRequest = () => {
  return {
    type: FETCH_LAZY_HOTELS_REQUEST
  }
}

export const findHotelsRequest = (value, history) => {  
  return {
    type: FIND_HOTELS_REQUEST,
    payload: {value, history}
  }
}

// SAGAS

export const fetchHotelSaga = function* (action) {
  const state = yield select();  

  if(state[moduleName].hotels.length > 0) {
    yield put({
      type: FETCH_HOTELS_SUCCESS,
      payload: state[moduleName].hotels
    })
    return;
  }

  const result = yield call(axios.get, `/api/hotel/?offset=0&limit=8`);

  try {    
    const { data } = result;
    yield put({
      type: FETCH_HOTELS_SUCCESS,
      payload: data
    })

  } catch (error) {
    yield put({
      type: FETCH_HOTELS_FAILURE,
      payload: error
    })
  }  
};

export const fetchLazyHotelSaga = function* (action) {
  const state = yield select();
  const { offset, limit } = state[moduleName];
  const newOffset = offset + limit; 

  try {
    const result = yield call(axios.get, `/api/hotel/?offset=${newOffset}&limit=8`);
    const { data } = result;
    
    yield put({
      type: FETCH_LAZY_HOTELS_SUCCESS,
      payload: data
    })

  } catch (error) {
    yield put({
      type: FETCH_LAZY_HOTELS_FAILURE,
      payload: error
    })
  }
};

export const findSaga = function* (action) {  
  const { value, history } = action.payload;
  try {
    const { data } = yield call(axios.post, '/api/hotel', value); 

    yield put({
      type: FIND_HOTELS_SUCCESS,
      payload: data
    })   

  } catch (error) {    
    yield put({
      type: FIND_HOTELS_FAILURE,
      payload: error.response.data.message
    })    
  }

  history.push('/search');
}

export const saga = function* () {
  yield all([
    takeEvery(FETCH_HOTELS_REQUEST, fetchHotelSaga),
    takeEvery(FETCH_LAZY_HOTELS_REQUEST, fetchLazyHotelSaga),
    takeEvery(FIND_HOTELS_REQUEST, findSaga)
  ])
};

// REDUCER

const HotelSchema = Record({
  offset: 0,
  limit: 8,
  isLoading: false,
  hotels: [],
  findHotels: [],
  error: null,
  loaded: false,
  lazyLoading: false,
  finding: false
});

export const reducer = (state = new HotelSchema(), action) => {
  const { type, payload } = action;
  switch(type) {
    case FETCH_HOTELS_REQUEST:
      return state
        .set('isLoading', true);
    case FETCH_HOTELS_SUCCESS:  
      const currentState = getPayload(state.hotels, payload);    
      return state
        .set('hotels', currentState.length > 0 ? currentState : payload)
        .set('isLoading', false) 
        .set('error', null);  
    case FETCH_HOTELS_FAILURE:
      return state
        .set('isLoading', false)         
        .set('error', payload);  

    case FETCH_LAZY_HOTELS_REQUEST:
      return state
        .set('lazyLoading', true);
    case FETCH_LAZY_HOTELS_SUCCESS:
      const newOffset = state.offset + state.limit;
      const newPayload = state.hotels.concat(payload);        
      return state
        .set('hotels', newPayload)
        .set('isLoading', false)  
        .set('offset', newOffset)
        .set('lazyLoading', false)
        .set('loaded', Object.keys(payload).length < 8)        
        .set('error', null);

    case FETCH_LAZY_HOTELS_FAILURE:
      return state
        .set('lazyLoading', false);    
    
    case FIND_HOTELS_REQUEST:
      return state
        .set('finding', true);
        
    case FIND_HOTELS_SUCCESS:
      return state
        .set('findHotels', payload)
        .set('error', null)
        .set('finding', false);   
        
    case FIND_HOTELS_FAILURE:
      return state
        .set('finding', [])
        .set('error', payload)
        .set('finding', false);  
        
    case 'RESET_STATE_OF_ERROR':
      return state
        .set('error', null)
          
    default:
      return state;  
  }
}