import React, { Component } from 'react';
import {connect} from 'react-redux';
import Loadable from 'react-loadable';
import Loader from '../components/shared/loader';
import { debounceFn } from '../utils/debounce';
import * as HotelsDuck from '../ducks/hotels';

const LoadableHeader = Loadable({
  loader: () => import('../components/shared/header'),
  loading: Loader,
});

const LoadableFooter = Loadable({
  loader: () => import('../components/shared/footer'),
  loading: Loader,
});

export const LoadableTravelList = Loadable({
  loader: () => import('../containers/main/TravelList'),
  loading: Loader,
});

class MainPage extends Component { 
  componentDidMount() {
    this.props.fetchHotelsRequest();    
    window.addEventListener('scroll', debounceFn(this.checkScroll, 60));
  }

  checkScroll = () => {
    const OFFSET = 140;
    const scrolled = Math.ceil(window.innerHeight + window.scrollY) + OFFSET;
    const maxHeightScroll = document.body.clientHeight;
    
    if(scrolled > maxHeightScroll) {
      this.props.findHotels.length === 0 && 
      !this.props.lazyLoading && 
      !this.props.loaded && 
      this.props.travels.length && 
      this.props.fetchLazyHotelsRequest();
    } 
  }

  render() {
    return (
      <div>
        <LoadableHeader 
          isAuthenticated={this.props.isAuthenticated} 
          find={this.props.findHotelsRequest}
          {...this.props}
        />
        <LoadableTravelList 
          travels={  this.props.travels && this.props.travels } 
          lazyLoading={this.props.lazyLoading}
        />
        <LoadableFooter />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  loaded: state[HotelsDuck.moduleName].loaded,
  lazyLoading: state[HotelsDuck.moduleName].lazyLoading,
  travels: HotelsDuck.getHotelList(state[HotelsDuck.moduleName]),
  findHotels: state[HotelsDuck.moduleName].findHotels
});

const { fetchHotelsRequest,  fetchLazyHotelsRequest, findHotelsRequest } = HotelsDuck;

const mapDispatchToProps = ({
  fetchHotelsRequest,
  fetchLazyHotelsRequest,
  findHotelsRequest
})

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
