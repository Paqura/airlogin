import React, { Component } from 'react';
import { connect } from 'react-redux';
import RegisterForm from '../containers/register';
import {registerFormRequest} from '../ducks/auth';

class Register extends Component {
  handleSubmit = (values) => {
    this.props.registerFormRequest(values, this.props.history);
  }

  render() {
    return (
      <div>
        <RegisterForm onSubmit={this.handleSubmit}/>
      </div>
    )
  }
}

const mapDispatchToProps = ({
  registerFormRequest
});

export default connect(null, mapDispatchToProps)(Register);
