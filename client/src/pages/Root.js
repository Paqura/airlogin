import React, { Component } from 'react';
import { Route, Switch } from 'react-router';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import MainPage from './MainPage';
import Toast from '../components/shared/toast';

import * as AuthDuck from '../ducks/auth';

import Loadable from 'react-loadable';
import Loader from '../components/shared/loader';

const LoadableHotel = Loadable({
  loader: () => import('./HotelPage'),
  loading: Loader,
});

const LoadableLogin = Loadable({
  loader: () => import('./LoginPage'),
  loading: Loader,
});

const LoadableRegister = Loadable({
  loader: () => import('./RegisterPage'),
  loading: Loader,
});

const LoadableSearch = Loadable({
  loader: () => import('./SearchPage'),
  loading: Loader,
});

class Root extends Component {  
  render() {    
    const { authError, resetStateOfError, isAuthenticated } = this.props;
    return (      
      <div className="app">
        <Switch>
          <Route exact 
            path="/" 
            render={() => 
              <MainPage 
                isAuthenticated={isAuthenticated} 
                {...this.props}
              />} 
          />
          <Route exact 
            path="/login" 
            component={LoadableLogin}
          />
          <Route exact 
            path="/register" 
            component={LoadableRegister}
          />
          <Route exact 
            path="/hotel/:id" 
            render={
              (props) => 
                <LoadableHotel 
                  isAuthenticated={isAuthenticated} 
                  {...props}
                />
            }          
          />
          <Route exact
            path="/search"
            render={() => 
              <LoadableSearch 
                isAuthenticated={isAuthenticated} 
                {...this.props}
              />} 
          />
        </Switch> 
        { authError && 
            <Toast 
              message={ authError} 
              action={resetStateOfError}
            /> }                      
      </div> 
    )
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state[AuthDuck.moduleName].isAuthenticated,
  authError: state[AuthDuck.moduleName].get('error')
});

const { resetStateOfError } = AuthDuck;

const mapDispatchToProps = ({
  resetStateOfError
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Root));
