import React, { Component } from 'react';
import { connect } from 'react-redux';
import Hotel from '../containers/hotel';
import Loader from '../components/shared/loader';
import * as HotelDuck from '../ducks/hotel';

class HotelPage extends Component {
  componentDidMount() {
    this.props.fetchOneHotel(this.props.match.params.id);
  }

  componentWillUnmount() {
    this.props.clearHotelState();
  }

  render() {
    const { hotelInfo, isAuthenticated } = this.props;

    return hotelInfo ? 
      <Hotel 
        hotel={hotelInfo} 
        isAuthenticated={isAuthenticated}
      /> :
      <Loader />      
  }
};

const mapStateToProps = state => ({
  hotelInfo: state[HotelDuck.moduleName].info
});

const { fetchOneHotel, clearHotelState } = HotelDuck;

const mapDispatchToProps = ({
  fetchOneHotel,
  clearHotelState
});

export default connect(mapStateToProps, mapDispatchToProps)(HotelPage);
