import React, { Component } from 'react';
import { connect } from 'react-redux';
import { loginFormRequest, moduleName } from '../ducks/auth';
import Loadable  from 'react-loadable';
import Loader from '../components/shared/loader';

const LoadableLoginForm = Loadable({
  loader: () => import('../containers/login'),
  loading: Loader,
});
class Login extends Component<{}, Props> {
  componentDidMount() {
    if(this.props.isAuthenticated) {
      this.props.history.push('/');
    }
  }
  
  handleSubmit = (values) => {
    this.props.loginFormRequest(values, this.props.history);
    
  }
  render() {
    return (
      <div>
        <LoadableLoginForm onSubmit={this.handleSubmit} submitState={this.props.submitState}/>       
      </div>
    )
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state[moduleName].isAuthenticated,
  submitState: state[moduleName].submit
});

const mapDispatchToProps = ({
  loginFormRequest
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
