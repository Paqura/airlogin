import React, { Component } from 'react';
import { connect } from 'react-redux';
import Header from '../containers/hotel/header';
import Search from '../containers/search/search';
import {LoadableTravelList} from './MainPage';
import * as HotelsDuck from '../ducks/hotels';

class SearchPage extends Component {
  render() {
    const {findHotels, isAuthenticated} = this.props;
    return (
      <div>
        <Header isAuthenticated={isAuthenticated}/>
        <Search find={this.props.findHotelsRequest} {...this.props}/>
        <LoadableTravelList 
          travels={findHotels && findHotels}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  findHotels: state[HotelsDuck.moduleName].findHotels,  
  searchError: state[HotelsDuck.moduleName].error,
  findHotelsRequest: state[HotelsDuck.moduleName].findHotelsRequest
});

const {findHotelsRequest} = HotelsDuck;

const mapDispatchToProps = ({
  findHotelsRequest
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchPage);
